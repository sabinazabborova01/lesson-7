// 
const minutes = document.querySelector('.minutes');
const seconds = document.querySelector('.seconds');

// Buttons
const start = document.querySelector('.start');
const pause = document.querySelector('.pause');
const reset = document.querySelector('.reset');

// 
start.addEventListener('click', () => {
    clearInterval(interval)
    interval = setInterval(startTime, 1000)
})

pause.addEventListener('click', () => {
    clearInterval(interval)
})

reset.addEventListener('click', () => {
    clearInterval(interval)
    clearTimer()
})

// Variables
let minute = 00,
    second = 00,
    interval
   
function startTime() {
    second++
    if(second < 9) {
        seconds.innerHTML = "0" + second
    }
    if(second > 9) {
        seconds.innerHTML = second
    }
    if(second > 59) {
        minute++
        minutes.innerHTML = "0" + minute
        second = 0
        seconds.innerHTML = "0" + second
    }
}

// 
function clearTimer() {
    minute = 00
    second = 00
    seconds.textContent = "00"
    minutes.textContent = "00"
}